from django.urls import path
from .views import main,main2,main3,had

urlpatterns = [
    path('',main ),
    path('num/',main2),
    path('true/',main3),
    path('sun/',had)
]    
